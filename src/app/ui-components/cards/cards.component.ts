import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})

export class CardsComponent implements OnInit {
  @Input('title') title: string;
  @Input('subtitle') subtitle: string;
  @Input('url') url: string;
  @Input('img') img: string;

  cards: Card[];
  pageData: PageData;
  // <mat-card-title>{{card.title}}</mat-card-title>
  // <mat-card-subtitle>{{card.subtitle}}</mat-card-subtitle>
  // <mat-card-content>{{card.content}}</mat-card-content>



  constructor() {
  }

  ngOnInit() {
    this.getPageData();
    this.getCards();
    console.log(this.title, this.subtitle, this.url)
  }

  getCards() {
    this.cards = CARDS;
  }

  getPageData() {
    this.pageData = new PageData();
    this.pageData.title = 'Абитуриенту';
    //this.pageData.content = 'Текст, подгружаемый из html-файла';
  }

}

export class Card {
  title: string;
  subtitle: string;
  content: string;
  photoUrl: string;
  link: string;
}

export class PageData {
  title: string;
  content: string; // урл на html страницу?
}

const CARDS = [
  {
    title: 'Информатика и вычислительная техника',
    subtitle: '09.03.01',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'https://2ch.hk/b/src/165020243/15107228561760.jpg',
    link: '#'
  },
  {
    title: 'Информационные системы и технологии',
    subtitle: '09.03.02',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'https://www.uriit.ru/upload/medialibrary/4e1/phpcut.jpg',
    link: '#'
  },
  {
    title: 'Прикладная информатика',
    subtitle: '09.03.03',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'http://www.tsit29.ru/upload/medialibrary/099/09929673e15735a86781c221c0ed8782.jpg',
    link: '#'
  },
  {
    title: 'Информационная безопасность',
    subtitle: '10.03.01',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'http://new-retail.ru/upload/iblock/343/343608f4efdc5388bf4fe063343bed2d.jpg',
    link: '#'
  },
  {
    title: 'Информатика и вычислительная техника',
    subtitle: '09.03.01',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'https://2ch.hk/b/src/165020243/15107228561760.jpg',
    link: '#'
  },
  {
    title: 'Информационные системы и технологии',
    subtitle: '09.03.02',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'https://www.uriit.ru/upload/medialibrary/4e1/phpcut.jpg',
    link: '#'
  },
  {
    title: 'Прикладная информатика',
    subtitle: '09.03.03',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'http://www.tsit29.ru/upload/medialibrary/099/09929673e15735a86781c221c0ed8782.jpg',
    link: '#'
  },
  {
    title: 'Информационная безопасность',
    subtitle: '10.03.01',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'http://new-retail.ru/upload/iblock/343/343608f4efdc5388bf4fe063343bed2d.jpg',
    link: '#'
  },
  {
    title: 'Информатика и вычислительная техника',
    subtitle: '09.03.01',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'https://2ch.hk/b/src/165020243/15107228561760.jpg',
    link: '#'
  },
  {
    title: 'Информационные системы и технологии',
    subtitle: '09.03.02',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'https://www.uriit.ru/upload/medialibrary/4e1/phpcut.jpg',
    link: '#'
  },
  {
    title: 'Прикладная информатика',
    subtitle: '09.03.03',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'http://www.tsit29.ru/upload/medialibrary/099/09929673e15735a86781c221c0ed8782.jpg',
    link: '#'
  },
  {
    title: 'Информационная безопасность',
    subtitle: '10.03.01',
    content: 'Дисциплины ЕГЭ: Математика, Русский язык, Информатика и информационно-компьютерные технологии',
    photoUrl: 'http://new-retail.ru/upload/iblock/343/343608f4efdc5388bf4fe063343bed2d.jpg',
    link: '#'
  }
];
