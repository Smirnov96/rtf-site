import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor(public thisDialogRef: MatDialogRef<ModalComponent>, @Inject(MAT_DIALOG_DATA) public data: object) { }

  ngOnInit() {
  }
  onCloseConfirm() {
    this.thisDialogRef.close();
  }

}
