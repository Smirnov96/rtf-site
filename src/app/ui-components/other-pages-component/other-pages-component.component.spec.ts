import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPagesComponentComponent } from './other-pages-component.component';

describe('OtherPagesComponentComponent', () => {
  let component: OtherPagesComponentComponent;
  let fixture: ComponentFixture<OtherPagesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherPagesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPagesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
