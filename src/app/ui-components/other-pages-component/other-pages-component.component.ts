import { Component, OnInit, Input } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-other-pages-component',
  templateUrl: './other-pages-component.component.html',
  styleUrls: ['./other-pages-component.component.css']
})
export class OtherPagesComponentComponent implements OnInit {
  page: string;
  
  constructor(private route: ActivatedRoute) {

   }
  //@Input() page: string;

  ngOnInit() {
    // this.changePage();
    this.route.params.subscribe((params: Params) => {
      this.page = params['page'];
      this.changePage();
    })
  }
  changePage() {
    document.getElementById('root-other-page').innerHTML = '<object type="text/html"' +'data=' +this.page + '.html' + ' style="width: 100%;height: 100%;" ></object>';
  }
}
//assets/other-pages/about/Accelerated_Learning_Center.html
