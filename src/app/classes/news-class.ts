export class NewsClass {
  title: string;
  description: string;
  img: [{
    url: string
  }];
}
