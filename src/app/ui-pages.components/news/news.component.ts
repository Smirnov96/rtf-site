import { Component, OnInit } from '@angular/core';
import {NewsClass} from '../../classes/news-class';
import {MatDialog} from '@angular/material';
import {UserService} from '../../services/user-service/user.service';
import {ModalComponent} from '../../ui-components/modal/modal.component';

@Component({
  selector: 'app-news-comp',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsPagesComponent implements OnInit {
  news: NewsClass;

  constructor(private userService: UserService, public dialog: MatDialog) { }

  ngOnInit() {
    console.log('asf');
    this.userService.getNews().subscribe(el => {
      this.news = el;
      console.log('was init');
    });
  }
  openModal(imgUrl: string): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '450px',
      data: { img: imgUrl }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
