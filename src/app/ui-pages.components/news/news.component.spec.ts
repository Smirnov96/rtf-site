import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsPagesComponent } from './news.component';

describe('NewsComponent', () => {
  let component: NewsPagesComponent;
  let fixture: ComponentFixture<NewsPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
