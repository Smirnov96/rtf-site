import { Injectable } from '@angular/core';
import {UserClass} from '../../classes/user-class';
import {HttpService} from '../http.service';

@Injectable()
export class UserService {
  localUser: UserClass;
  constructor(private http: HttpService) { }

  public setUser(user: UserClass) {
    if ( user ) {
      console.log('done');
      this.localUser = user;
      return true;
    } else {
      console.log('saving error');
      return false;
    }
  }
  public getUser() {
    if ( this.localUser ) {
      return this.localUser;
    } else {
      return null;
    }
  }

  public getNews() {
    return this.http.getNews();
  }

}
