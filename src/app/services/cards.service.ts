import { Injectable } from '@angular/core';

class Card {
  title: string;
  img: string;
  link: string;
  description: string;

  constructor(title: string, img: string, link: string, description: string) {
    this.title = title;
    this.img = img;
    this.link = link;
    this.description = description;
  }

  toString() {
    return 'title:' + this.title + ' img:' + this.img + ' link' + this.link + ' description: ' + this.description;
  }
}

@Injectable()
export class CardsService {

  constructor() {
  }

  getCardsForAbout() {
    return [
      new Card('Title1', 'link\\to\\img1', 'link\\to\\other\\page1', 'description'),
      new Card('Title2', 'link\\to\\img2', 'link\\to\\other\\page2', 'description'),
      new Card('Title2', 'link\\to\\img3', 'link\\to\\other\\page3', 'description'),
      new Card('Title3', 'link\\to\\img4', 'link\\to\\other\\page4', 'description'),
      new Card('Title4', 'link\\to\\img5', 'link\\to\\other\\page5', 'description'),
      new Card('Title5', 'link\\to\\img6', 'link\\to\\other\\page6', 'description'),
      new Card('Title6', 'link\\to\\img7', 'link\\to\\other\\page7', 'description'),
      new Card('Title7', 'link\\to\\img8', 'link\\to\\other\\page8', 'description')
    ];
  }

}
