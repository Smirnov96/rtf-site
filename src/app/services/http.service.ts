import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as config from '../config.js';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {UserClass} from '../classes/user-class';
import {NewsClass} from '../classes/news-class';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) {
  }
  public auth(data: Object): Observable<any> {
    console.log(data);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post<UserClass>('https://reqres.in/api/users', data, );
  }
  public getNews() {
    return this.http.get<NewsClass>('assets/news.json');
  }
}



