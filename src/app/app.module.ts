import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Router,ActivatedRoute,RouterModule, ParamMap, Routes } from '@angular/router';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';



import { AppComponent } from './app.component';



/*ui-componensts */
import { FooterComponent } from './ui-components/footer/footer.component';
import { HeaderComponent } from './ui-components/header/header.component';
import { CardsComponent } from './ui-components/cards/cards.component';
import { ModalComponent } from './ui-components/modal/modal.component';

/* —-------------------------------------------------— */

/*ui-pages.componensts */
import { TextsComponent } from './ui-pages.components/texts/texts.component';
import { BannersComponent } from './ui-pages.components/banners/banners.component';
import { Tab1Component } from './ui-pages.components/tab1/tab1.component';
import { Tab2Component } from './ui-pages.components/tab2/tab2.component';
import { NewsPagesComponent } from './ui-pages.components/news/news.component';
import { GroupsScheduleComponent } from './ui-pages.components/groups-schedule/groups-schedule.component';

/* —-------------------------------------------------— */

/*pages */
import { MainPageComponent } from './pages/main-page/main-page.component';
import { EnrolleePageComponent } from './pages/enrollee-page/enrollee-page.component';
import { StudentPageComponent } from './pages/student-page/student-page.component';
import { AboutComponent } from './pages/about/about.component';
import { AspirComponent } from './pages/aspir/aspir.component';
import { NewsComponent } from './pages/news/news.component';
import { SienceComponent } from './pages/sience/sience.component';
import { WorkComponent } from './pages/work/work.component';
import { NotStudComponent } from './pages/not-stud/not-stud.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AdminAuthComponent } from './pages/admin-auth/admin-auth.component';
/* —-------------------------------------------------— */

/* Angular Material and Bootstrap*/
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonModule} from '@angular/material/button';
import {HttpService} from './services/http.service';
import {UserService} from './services/user-service/user.service';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {CardsService} from './services/cards.service';
import { OtherPagesComponentComponent } from './ui-components/other-pages-component/other-pages-component.component';


/*------------------------------------------------------*/

const appRouting: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'for-abit', component: EnrolleePageComponent },
  { path: 'for-students', component: StudentPageComponent },
  { path: 'admin/a', component: AdminAuthComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'about', component: AboutComponent },
  { path: 'not-stud', component: NotStudComponent },
  { path: 'page/:page', component: OtherPagesComponentComponent },
  { path: 'science', component: SienceComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    EnrolleePageComponent,
    StudentPageComponent,
    FooterComponent,
    HeaderComponent,
    AboutComponent,
    AspirComponent,
    NewsComponent,
    SienceComponent,
    WorkComponent,
    NotStudComponent,
    AdminComponent,
    AdminAuthComponent,
    CardsComponent,
    ModalComponent,
    TextsComponent,
    BannersComponent,
    Tab1Component,
    Tab2Component,
    NewsPagesComponent,
    GroupsScheduleComponent,
    OtherPagesComponentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRouting),
    MatInputModule,
    MatIconModule,
    MatSlideToggleModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    NgbModule.forRoot(),
    MatTabsModule,
    MatDialogModule
],
  entryComponents: [
    ModalComponent
  ],
  providers: [
    Title,
    HttpService,
    UserService,
    CardsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
