import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SienceComponent } from './sience.component';

describe('SienceComponent', () => {
  let component: SienceComponent;
  let fixture: ComponentFixture<SienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
