import { Component, OnInit } from '@angular/core';
import {UserClass} from '../../classes/user-class';
import {UserService} from '../../services/user-service/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  user: UserClass;
  constructor(private userService: UserService,) { }

  ngOnInit() {
    this.user = this.userService.getUser();
    console.log(this.user);

  }



}
