import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpService} from '../../services/http.service';
import {Router} from '@angular/router';
import {UserClass} from '../../classes/user-class';
import {UserService} from '../../services/user-service/user.service';

@Component({
  selector: 'app-admin-auth',
  templateUrl: './admin-auth.component.html',
  styleUrls: ['./admin-auth.component.css']
})
export class AdminAuthComponent implements OnInit {
  user: UserClass;
  public validError: string;
  public authForm: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.minLength(4),
      Validators.maxLength(50),
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.minLength(4),
      Validators.maxLength(50),
      Validators.required
    ])
  });

  constructor(private _httpService: HttpService, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }
  initAuth() {
    if ( this.authForm.valid ) {
      // const authObject = {
      //   username: this.authForm.controls.username.value,
      //   password: this.authForm.controls.password.value
      // };
      const authObject = {
        name: this.authForm.controls.username.value,
        job: this.authForm.controls.password.value
      };
      this._httpService.auth(authObject).subscribe(res => {
          if ( res.id ) {
            const service_res = this.userService.setUser(res);
            console.log(service_res);
            if ( service_res ) {
              this.router.navigate(['/admin'], { replaceUrl: true} );
            }
          }
      });
    } else {
      this.validError = 'Заполните форму.';
      setTimeout(() => {
        this.validError = null;
      }, 1500);
    }
  }
}
