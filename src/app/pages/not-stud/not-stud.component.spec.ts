import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotStudComponent } from './not-stud.component';

describe('NotStudComponent', () => {
  let component: NotStudComponent;
  let fixture: ComponentFixture<NotStudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotStudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotStudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
