import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AspirComponent } from './aspir.component';

describe('AspirComponent', () => {
  let component: AspirComponent;
  let fixture: ComponentFixture<AspirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AspirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AspirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
