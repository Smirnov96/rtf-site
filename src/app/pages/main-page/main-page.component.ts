import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

class News {
  date: string;
  title: string;
  img: string;
  url: string;

  constructor(date: string, title: string, img: string, url: string) {
    this.date = date;
    this.title = title;
    this.img = img;
    this.url = url;
  }
}

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  news: News[] = [];

  constructor(private titleService: Title) {
    this.news = [ new News(
      '14 апреля',
      'Семинар на тему Элементы теории антагонистических игр',
      'assets/img/main/1.jpg',
      '#'),
      new News(
        '14 апреля',
        'Семинар на тему Элементы теории антагонистических игр',
        'assets/img/main/1.jpg',
        '#'),
      new News(
        '14 апреля',
        'Семинар на тему Элементы теории антагонистических игр',
        'assets/img/main/5.jpg',
        '#'),
      new News(
        '14 апреля',
        'Семинар на тему Элементы теории антагонистических игр',
        'assets/img/main/1.jpg',
        '#'),
      new News(
        '14 апреля',
        'Семинар на тему Элементы теории антагонистических игр',
        'assets/img/main/5.jpg',
        '#'),
      new News(
        '14 апреля',
        'Семинар на тему Элементы теории антагонистических игр',
        'assets/img/main/5.jpg',
        '#')
    ];
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
  ngOnInit() {
    this.setTitle('Главная');
  }

}
